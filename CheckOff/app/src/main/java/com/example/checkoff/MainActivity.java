package com.example.checkoff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import java.util.ArrayList;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {
    class Listener extends RecyclerViewAdapter {
        @Override
        public void testMethod(int pos) {
            //super.testMethod(pos);
            System.out.println("Clicking on an item " + pos);
            removeAndEditItem(pos);
        }

        @Override
        public void checkBoxMethod(int pos) {
            activeListManager.toggleCheck(activeListPosition, pos);
            System.out.println("What up big dog " + pos);
            populateRecyclerView();
        }
    }

    class ListenerLM extends RecyclerViewAdapterLM {
        @Override
        public void testMethod(int pos) {
            //super.testMethod(pos);
            System.out.println("Clicking on a list " + pos);
            removeAndEditList(pos);
        }
    }

    // System variables;
    private ListManager activeListManager = new ListManager();
    private Integer activeListPosition = 0;
    private Integer editDeletePosition = 0;
    private Listener itemAdapter = new Listener();
    private ListenerLM lmAdapter = new ListenerLM();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        populateListManager();
        setSpinner();

    }

    // Used for testing
    private void populateListManager() {
        activeListManager.addList("Walmart", "");
        activeListManager.addList("Staples", "Back to school shopping");
        activeListManager.addItem(0, "Bread", "Honey wheat", "1 loaf");
        activeListManager.addItem(0, "Cheese", "Cheddar", "");
        activeListManager.addItem(0, "Eggs", "", "12");
        activeListManager.addItem(0, "Apples", "Gala", "5");
        activeListManager.addItem(1, "Mechanical pencils", "", "");
        activeListManager.addItem(1, "Pencil lead", "0.7 mm", "");
        activeListManager.addItem(1, "Notebooks", "", "5");
        activeListManager.addItem(1, "Loose-leaf paper", "College Ruled", "2 packs");
    }

    //********************* METHODS FOR CREATING DYNAMIC LISTS ***********************************//
    // This method is responsible for creating the list dropdown menu
    private void setSpinner() {
        ArrayList<String> listNames = new ArrayList<String>();

        for (int i = 0; i < activeListManager.masterList.size(); i++) {
            listNames.add((i + 1) +". " + activeListManager.masterList.get(i).getName());
        }

        Spinner spinner = (Spinner)findViewById(R.id.textListName);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(MainActivity.this,
                R.layout.dropdown_item,listNames);

        adapter.setDropDownViewResource(R.layout.dropdown_list_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(activeListPosition);

        // Update the list to display correctly when a new list is chosen
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                activeListPosition = i;
                populateRecyclerView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    // This method is responsible for creating the list view on the home screen
    private void populateRecyclerView()
    {
        Vector<Item> activeItems = new Vector<>();
        if (activeListManager.masterList.size()!=0) {
            activeItems = activeListManager.masterList.get(activeListPosition).items;
            RecyclerView recyclerView = findViewById(R.id.listView);
            itemAdapter.removeItems();
            itemAdapter.addItems(activeItems);
            recyclerView.setAdapter(itemAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    // This method is responsible for creating the list manager view in the list manager screen
    private void populateRecyclerViewLM()
    {
        Vector<List> activeLists = new Vector<>();
        if (activeListManager.masterList.size()!=0) {
            activeLists = activeListManager.masterList;
            RecyclerView recyclerView = findViewById(R.id.listManagerView);
            lmAdapter.removeLists();
            lmAdapter.addLists(activeLists);
            recyclerView.setAdapter(lmAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    //********************* METHODS FOR SWITCHING IN BETWEEN SCREENS *****************************//
    // This is the onClick method for switching to the to the Add Item screen
    public void switchToAddItem(View view)
    {
        setContentView(R.layout.add_item);
        TextView addItemsPrompt = findViewById(R.id.addNewItemsPrompt);
        addItemsPrompt.setText("Add new items to " + activeListManager.masterList.get(activeListPosition).getName());
    }

    // This is the onClick method for switching to the Add List screen
    public void switchToAddList(View view)
    {
        setContentView(R.layout.add_list);
    }

    // This is the onClick method for switching back to the home screen without adding, editing, or deleting items
    public void backToMainScreen(View view)
    {
        setContentView(R.layout.activity_main);
        setSpinner();
    }

    // This is the onClick method for switching back to the home screen when finished adding items
    public void doneAddingItems(View view)
    {
        TextView fromUIName = findViewById(R.id.toAddName);
        CharSequence toBackendName = fromUIName.getText();
        TextView fromUIDesc = findViewById(R.id.toAddDesc);
        CharSequence toBackendDesc = fromUIDesc.getText();
        TextView fromUIQty = findViewById(R.id.toAddQty);
        CharSequence toBackendQty = fromUIQty.getText();
        // In the future, we'll want to throw an error message here if the string equals "", but for now, just don't add the item and return to the home screen
        if (!"".equals(toBackendName.toString()))
        {
            activeListManager.addItem(activeListPosition, toBackendName.toString(), toBackendDesc.toString(), toBackendQty.toString());
        }
        setContentView(R.layout.activity_main);
        setSpinner();
    }

    // This is the onClick method for saving the current item and adding another one
    public void addAnotherItem(View view)
    {
        TextView fromUIName = findViewById(R.id.toAddName);
        CharSequence toBackendName = fromUIName.getText();
        fromUIName.setText("");
        TextView fromUIDesc = findViewById(R.id.toAddDesc);
        CharSequence toBackendDesc = fromUIDesc.getText();
        fromUIDesc.setText("");
        TextView fromUIQty = findViewById(R.id.toAddQty);
        CharSequence toBackendQty = fromUIQty.getText();
        fromUIQty.setText("");
        // In the future, we'll want to throw an error message here if the string equals "", but for now, just don't add the item and clear the screen
        if (!"".equals(toBackendName.toString()))
        {
            activeListManager.addItem(activeListPosition, toBackendName.toString(), toBackendDesc.toString(), toBackendQty.toString());
        }
    }

    // This is the onClick method for switching back to the List Manager screen without adding a list
    public void backToListManager(View view)
    {
        setContentView(R.layout.list_manager);
        populateRecyclerViewLM();
    }

    // This is the onClick method for switching back to the List Manager screen after adding a list
    public void doneAddingList(View view)
    {
        TextView fromUIName = findViewById(R.id.toAddListName);
        CharSequence toBackendName = fromUIName.getText();
        TextView fromUIDesc = findViewById(R.id.toAddListDesc);
        CharSequence toBackendDesc = fromUIDesc.getText();
        if (!"".equals(toBackendName.toString())){
            activeListManager.addList(toBackendName.toString(), toBackendDesc.toString());
        }
        setContentView(R.layout.list_manager);
        populateRecyclerViewLM();
    }

    // This is the onClick method for switching to the list manager screen
    public void switchToListManager(View view)
    {
        setContentView(R.layout.list_manager);
        populateRecyclerViewLM();
    }

    // This is the onClick method for moving to the edit/remove item screen
    public void removeAndEditItem(Integer pos)
    {
        Item toEdit = activeListManager.masterList.get(activeListPosition).items.get(pos);
        editDeletePosition = pos;
        String curName = toEdit.getName();
        String curDesc = toEdit.getDescription();
        String curQty = toEdit.getQty();
        setContentView(R.layout.edit_delete_item);
        TextView editNameView = findViewById(R.id.toEditItemName);
        TextView editDescView = findViewById(R.id.toEditItemDesc);
        TextView editQtyView = findViewById(R.id.toEditItemQty);
        editNameView.setText(curName);
        editDescView.setText(curDesc);
        editQtyView.setText(curQty);
    }

    // This is the onClick method for removing an item
    public void removeItem(View view)
    {
        activeListManager.removeItem(activeListPosition, editDeletePosition.toString());
        setContentView(R.layout.activity_main);
        setSpinner();
    }

    // This is the onClick method for saving an item's edits
    public void doneEditingItem(View view)
    {
        TextView fromUIName = findViewById(R.id.toEditItemName);
        CharSequence toBackendName = fromUIName.getText();
        TextView fromUIDesc = findViewById(R.id.toEditItemDesc);
        CharSequence toBackendDesc = fromUIDesc.getText();
        TextView fromUIQty = findViewById(R.id.toEditItemQty);
        CharSequence toBackendQty = fromUIQty.getText();
        // In the future, we'll want to throw an error message here if the string equals "", but for now, just don't add the item and return to the home screen
        if (!"".equals(toBackendName.toString()))
        {
            activeListManager.editItem(activeListPosition, editDeletePosition, toBackendName.toString(), toBackendDesc.toString(), toBackendQty.toString());
        }
        setContentView(R.layout.activity_main);
        setSpinner();
    }

    // This is the onClick method for moving to the edit/remove item screen
    public void removeAndEditList(Integer pos)
    {
        List toEdit = activeListManager.masterList.get(pos);
        editDeletePosition = pos;
        String curName = toEdit.getName();
        String curDesc = toEdit.getDescription();
        setContentView(R.layout.edit_delete_list);
        TextView editNameView = findViewById(R.id.toEditListName);
        TextView editDescView = findViewById(R.id.toEditListDesc);
        editNameView.setText(curName);
        editDescView.setText(curDesc);
    }

    // This is the onClick method for removing a list
    public void removeList(View view)
    {
        activeListManager.removeList(editDeletePosition);
        if (editDeletePosition <= activeListPosition)
        {
            activeListPosition--;
        }
        setContentView(R.layout.list_manager);
        populateRecyclerViewLM();
    }

    // This is the onClick method for saving a list's edits
    public void doneEditingList(View view)
    {
        TextView fromUIName = findViewById(R.id.toEditListName);
        CharSequence toBackendName = fromUIName.getText();
        TextView fromUIDesc = findViewById(R.id.toEditListDesc);
        CharSequence toBackendDesc = fromUIDesc.getText();
        // In the future, we'll want to throw an error message here if the string equals "", but for now, just don't add the item and return to the home screen
        if (!"".equals(toBackendName.toString()))
        {
            activeListManager.editList(editDeletePosition, toBackendName.toString(), toBackendDesc.toString());
        }
        setContentView(R.layout.list_manager);
        populateRecyclerViewLM();
    }
}
