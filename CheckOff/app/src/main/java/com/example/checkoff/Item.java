package com.example.checkoff;

public class Item {
    public Item(String itemName) {
        name = itemName;
    }
    public Item(String itemName, String itemDesc, String itemQty) {
        name = itemName;
        description = itemDesc;
        qty = itemQty;
    }
    public Item(String itemName, String itemDesc, String itemQty, Boolean checked) {
        name = itemName;
        description = itemDesc;
        qty = itemQty;
        check = checked;
    }


    public String getName() {return name;}
    public void setName(String itemName) {name = itemName;}
    public String getDescription() {return description;}
    public void setDescription(String itemDescription) {description = itemDescription;}
    public String getQty() {return qty;}
    public void setQty(String itemQty) {qty = itemQty;}
    public Boolean getCheck() {return check;}
    public void toggleCheck() {check = !check;}

    private String name;
    private String description;
    private String qty;
    private Boolean check = false;
}