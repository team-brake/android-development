package com.example.checkoff;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Vector;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mDescs = new ArrayList<>();
    private ArrayList<String> mQtys = new ArrayList<>();
    private ArrayList<Boolean> mChecks = new ArrayList<>();
    private Integer activePosition;

    public RecyclerViewAdapter(Vector<Item> currentList)
    {
        for (int k=0; k<currentList.size(); k++){
            mNames.add(currentList.get(k).getName());
            mDescs.add(currentList.get(k).getDescription());
            mQtys.add(currentList.get(k).getQty());
            mChecks.add(currentList.get(k).getCheck());
        }
    }

    public RecyclerViewAdapter() { }

    public void addItems(Vector<Item> currentList) {
        for (int k=0; k<currentList.size(); k++){
            mNames.add(currentList.get(k).getName());
            mDescs.add(currentList.get(k).getDescription());
            mQtys.add(currentList.get(k).getQty());
            mChecks.add(currentList.get(k).getCheck());
        }
    }

    public void removeItems() {
        mNames.clear();
        mDescs.clear();
        mQtys.clear();
        mChecks.clear();
    }

    public Integer getActivePosition() {
        return activePosition;
    }

    public void testMethod(int pos) {

    }

    public void checkBoxMethod(int pos) {

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.name.setText(mNames.get(position));
        holder.desc.setText(mDescs.get(position));
        holder.qty.setText(mQtys.get(position));
        holder.check.setChecked(mChecks.get(position));
    }

    @Override
    public int getItemCount() {
        return mNames.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        TextView desc;
        TextView qty;
        CheckBox check;
        ConstraintLayout parentLayout;

        public ViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.itemName);
            desc = itemView.findViewById(R.id.itemDescription);
            qty = itemView.findViewById(R.id.itemQty);
            check = itemView.findViewById(R.id.checkBox);
            parentLayout = itemView.findViewById(R.id.itemTemplate);

            check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();

                    // check if item still exists
                    if (pos != RecyclerView.NO_POSITION){
                        System.out.println("Click: " + pos);
                        activePosition = pos;
                        RecyclerViewAdapter.this.checkBoxMethod(pos);
                    }
                }
            });

            // on item click
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // print out item position
                    int pos = getAdapterPosition();

                    // check if item still exists
                    if (pos != RecyclerView.NO_POSITION){
                        System.out.println("Click: " + pos);
                        activePosition = pos;
                    }
                }
            });

            // on item hold
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view){
                    // get position
                    int pos = getAdapterPosition();

                    // print out item position
                    if (pos != RecyclerView.NO_POSITION){
                        System.out.println("Long Click: " + pos);
                        activePosition = pos;
                        RecyclerViewAdapter.this.testMethod(pos);
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            });
        }

    }

}
