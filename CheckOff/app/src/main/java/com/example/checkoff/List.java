package com.example.checkoff;

import java.util.Vector;

public class List {
    public List(String listName) {
        name = listName;
        items = new Vector<Item>();
        checkedPositionStart = 0;
    }

    public List(String listName, String listDesc) {
        name = listName;
        description = listDesc;
        items = new Vector<Item>();
        checkedPositionStart = 0;
    }

    public String getName() {return name;}
    public void setName(String listName) {name = listName;}
    public String getDescription() {return description;}
    public void setDescription(String listDescription) {description = listDescription;}

    private String name;
    private String description;
    private Integer checkedPositionStart;  // position where the checked portion of the list begins. This is needed to properly move items to the correct spot when checked off
    public Vector<Item> items;
}
