package com.example.checkoff;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Vector;

public class RecyclerViewAdapterLM extends RecyclerView.Adapter<RecyclerViewAdapterLM.ViewHolder> {

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mDescs = new ArrayList<>();
    private Integer activePosition;

    public RecyclerViewAdapterLM(Vector<List> currentListManager)
    {
        for (int k=0; k<currentListManager.size(); k++){
            mNames.add(currentListManager.get(k).getName());
            mDescs.add(currentListManager.get(k).getDescription());
        }
    }

    public void testMethod(int pos)
    {

    }


    public RecyclerViewAdapterLM() { }

    public Integer getActivePosition() {
        return activePosition;
    }

    public void addLists(Vector<List> currentListManager) {
        for (int k=0; k<currentListManager.size(); k++){
            mNames.add(currentListManager.get(k).getName());
            mDescs.add(currentListManager.get(k).getDescription());
        }
    }

    public void removeLists() {
        mNames.clear();
        mDescs.clear();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_manager_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.name.setText((position + 1) +".   " + mNames.get(position));
        holder.desc.setText(mDescs.get(position));
    }

    @Override
    public int getItemCount() {
        return mNames.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        TextView desc;
        ConstraintLayout parentLayout;

        public ViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.textListName);
            desc = itemView.findViewById(R.id.textListDescription);
            parentLayout = itemView.findViewById(R.id.listTemplate);

            // on item click
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // print out item position
                    int pos = getAdapterPosition();

                    // check if item still exists
                    if (pos != RecyclerView.NO_POSITION){
                        System.out.println("Click: " + pos);
                        activePosition = pos;
                    }
                }
            });

            // on item hold
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view){
                    // get position
                    int pos = getAdapterPosition();

                    // print out item position
                    if (pos != RecyclerView.NO_POSITION){
                        System.out.println("Long Click: " + pos);
                        activePosition = pos;
                        RecyclerViewAdapterLM.this.testMethod(pos);
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            });
        }


    }

}
