package com.example.checkoff;

import java.util.Vector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ListManager {
    // Used to load the 'common-code' library on application startup.
    static {
        System.loadLibrary("common-code");
    }

    public ListManager() {
        masterList = new Vector<List>();
    }

    public ListManager(String JSONString) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();

        Gson gson = builder.create();
        ListManager buildup = gson.fromJson(JSONString, ListManager.class);
        this.masterList = buildup.masterList;
    }

    public String toJSONString() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();

        Gson gson = builder.create();
        return gson.toJson(this);
    }

    public void addList(String name, String description) {
        String jJson = addList(toJSONString(), name, description);
        ListManager buildup = new ListManager(jJson);
        this.masterList = buildup.masterList;
    }

    public void moveList(Integer to, Integer from) {
        String jJson = moveList(toJSONString(), to.toString(), from.toString());
        ListManager buildup = new ListManager(jJson);
        this.masterList = buildup.masterList;
    }

    public void editList(Integer location, String name, String description) {
        String jJson = editList(toJSONString(), location.toString(), name, description);
        ListManager buildup = new ListManager(jJson);
        this.masterList = buildup.masterList;
    }

    public void removeList(Integer location) {
        String jJson = removeList(toJSONString(), location.toString());
        ListManager buildup = new ListManager(jJson);
        this.masterList = buildup.masterList;
    }

    public void addItem(Integer listLocation, String itemName, String itemDescription, String itemQty) {
        String jJson = addItem(toJSONString(), listLocation.toString(), itemName, itemDescription, itemQty);
        ListManager buildup = new ListManager(jJson);
        this.masterList = buildup.masterList;
    }

    public void toggleCheck(Integer listLocation, Integer itemLocation) {
        String jJson = toggleCheck(toJSONString(), listLocation.toString(), itemLocation.toString());
        ListManager buildup = new ListManager(jJson);
        this.masterList = buildup.masterList;
    }

    public void moveItem(Integer listLocation, String itemTo, String itemFrom) {
        String jJson = moveItem(toJSONString(), listLocation.toString(), itemTo.toString(), itemFrom.toString());
        ListManager buildup = new ListManager(jJson);
        this.masterList = buildup.masterList;
    }

    public void editItem(Integer listLocation, Integer itemLocation, String itemName, String itemDescription, String itemQty) {
        String jJson = editItem(toJSONString(), listLocation.toString(), itemLocation.toString(), itemName, itemDescription, itemQty);
        ListManager buildup = new ListManager(jJson);
        this.masterList = buildup.masterList;
    }

    public void removeItem(Integer listLocation, String itemLocation) {
        String jJson = removeItem(toJSONString(), listLocation.toString(), itemLocation.toString());
        ListManager buildup = new ListManager(jJson);
        this.masterList = buildup.masterList;
    }

    private native String addList(String jJSON, String name, String description);
    private native String moveList(String jJSON, String jTo, String jFrom);
    private native String editList(String jJSON, String jLocation, String jName, String jDescription);
    private native String removeList(String jJSON, String jLocation);

    private native String addItem(String jJSON, String jListLocation, String jItemName, String jItemDescription, String jItemQty);
    private native String toggleCheck(String jJSON, String jListLocation, String jItemLocation);
    private native String moveItem(String jJSON, String jListLocation, String jItemTo, String jItemFrom);
    private native String editItem(String jJSON, String jListLocation, String jItemLocation, String jItemName, String jItemDescription, String jItemQty);
    private native String removeItem(String jJSON, String jListLocation, String jItemLocation);

    public Vector<List> masterList;
}
