#include "Headers/Serializer.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/document.h"

using namespace rapidjson;
using namespace std;


Serializer::Serializer() {
	// Empty for now, may need to change this later
}

void ListManagerSerializer::serialize(Writer<StringBuffer>& writer) {
	writer.StartObject();
	writer.Key("masterList");
	vector<List> lists = manager.getAllLists();
	writer.StartArray();
	for (int i = 0; i < lists.size(); i++) {
		writer.StartObject();
		writer.Key("name");
		writer.String(lists[i].getName().c_str());
		writer.Key("description");
		writer.String(lists[i].getDescription().c_str());
		writer.Key("checkedPositionStart");
		writer.Int(lists[i].getCheckedPositionStart());

		writer.Key("items");
		vector<Item> items = lists[i].getAllItems();
		writer.StartArray();
		for (int j = 0; j < items.size(); j++) {
			writer.StartObject();
			writer.Key("name");
			writer.String(items[j].getName().c_str());
			writer.Key("description");
			writer.String(items[j].getDescription().c_str());
			writer.Key("qty");
			writer.String(items[j].getQty().c_str());
			writer.Key("check");
			writer.Bool(items[j].getCheck());
			writer.EndObject();
		}
		writer.EndArray();

		writer.EndObject();
	}
	writer.EndArray();

	writer.EndObject();
}

ListManagerSerializer::ListManagerSerializer(ListManager listManager) : manager(listManager) {
	StringBuffer s;
	Writer<StringBuffer> writer(s);
	serialize(writer);
	json = s.GetString();
}

void ListManagerSerializer::parse() {
    Document document;
    document.Parse(this->json.c_str());
	
	const Value& masterList = document["masterList"];

	for (SizeType i = 0; i < masterList.Size(); i++) {
		AddList addList(masterList[i]["name"].GetString(), masterList[i]["description"].GetString(), manager);
		
		const Value& items = masterList[i]["items"];
		List* curr_list = manager.getListAddress(i);

		for (SizeType j = 0; j < items.Size(); j++) {
			string name = items[j]["name"].GetString();
			string description = items[j]["description"].GetString();
			string qty = items[j]["qty"].GetString();
			bool check = items[j]["check"].GetBool();
			AddItem addItem(name, description, qty, check, *curr_list);
		}
		curr_list->setCheckedPositionStart(masterList[i]["checkedPositionStart"].GetInt());
	}
}

ListManagerSerializer::ListManagerSerializer(string json_str) {
    json = json_str;
    parse();
}