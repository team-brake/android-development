#include <jni.h>
#include "Headers/Serializer.h"

using namespace std;

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_checkoff_ListManager_addList (JNIEnv *env, jobject, jstring jJson, jstring name, jstring description) {
    //Convert java type variables into c++ type variables
    const char *cppJson = env->GetStringUTFChars(jJson, NULL);
	string cppName(env->GetStringUTFChars(name, NULL));
	string cppDes(env->GetStringUTFChars(description, NULL));

    //Build the manager using the Json string
    ListManager manager = ListManagerSerializer(cppJson).manager;

    //Do the appropriate action
    AddList adder(cppName, cppDes, manager);

    //Convert manager to c++ json string
    string cJson = ListManagerSerializer(manager).json;

    // Converting C++ String to Java String and return it
    return env->NewStringUTF(cJson.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_checkoff_ListManager_moveList (JNIEnv *env, jobject, jstring jJson, jstring jTo, jstring jFrom) {
    //Convert java type variables into c++ type variables
    const char *cppJson = env->GetStringUTFChars(jJson, NULL);
    int cppTo= atoi(env->GetStringUTFChars(jTo, NULL));
    int cppFrom = atoi(env->GetStringUTFChars(jFrom, NULL));

    //Build the manager using the Json string
    ListManager manager = ListManagerSerializer(cppJson).manager;

    //Do the appropriate action
    MoveList moveList(cppTo, cppFrom, manager);

    //Convert manager to c++ json string
    string cJson = ListManagerSerializer(manager).json;

    // Converting C++ String to Java String and return it
    return env->NewStringUTF(cJson.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_checkoff_ListManager_editList (JNIEnv *env, jobject, jstring jJson, jstring jLocation, jstring jName, jstring jDescription) {
    //Convert java type variables into c++ type variables
    const char *cppJson = env->GetStringUTFChars(jJson, NULL);
    int cppLocation = atoi(env->GetStringUTFChars(jLocation, NULL));
    string cppName(env->GetStringUTFChars(jName, NULL));
    string cppDes(env->GetStringUTFChars(jDescription, NULL));

    //Build the manager using the Json string
    ListManager manager = ListManagerSerializer(cppJson).manager;

    //Do the appropriate action
    EditList editList(cppLocation, cppName, cppDes, manager);

    //Convert manager to c++ json string
    string cJson = ListManagerSerializer(manager).json;

    // Converting C++ String to Java String and return it
    return env->NewStringUTF(cJson.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_checkoff_ListManager_removeList (JNIEnv *env, jobject, jstring jJson, jstring jLocation) {
    //Convert java type variables into c++ type variables
    const char *cppJson = env->GetStringUTFChars(jJson, NULL);
    int cppLocation = atoi(env->GetStringUTFChars(jLocation, NULL));

    //Build the manager using the Json string
    ListManager manager = ListManagerSerializer(cppJson).manager;

    //Do the appropriate action
    RemoveList remover(cppLocation, manager);

    //Convert manager to c++ json string
    string cJson = ListManagerSerializer(manager).json;

    // Converting C++ String to Java String and return it
    return env->NewStringUTF(cJson.c_str());
}


extern "C" JNIEXPORT jstring JNICALL
Java_com_example_checkoff_ListManager_addItem (JNIEnv *env, jobject, jstring jJson, jstring jListLocation, jstring jItemName, jstring jItemDescription, jstring jItemQty) {
    //Convert java type variables into c++ type variables
    const char *cppJson = env->GetStringUTFChars(jJson, NULL);
    int cppListLocation = atoi(env->GetStringUTFChars(jListLocation, NULL));
    string cppItemName(env->GetStringUTFChars(jItemName, NULL));
    string cppItemDescription(env->GetStringUTFChars(jItemDescription, NULL));
    string cppItemQty(env->GetStringUTFChars(jItemQty, NULL));

    //Build the manager using the Json string
    ListManager manager = ListManagerSerializer(cppJson).manager;

    //Do the appropriate action
    List* curr_list = manager.getListAddress(cppListLocation);
    AddItem addItem(cppItemName, cppItemDescription, cppItemQty, *curr_list);

    //Convert manager to c++ json string
    string cJson = ListManagerSerializer(manager).json;

    // Converting C++ String to Java String and return it
    return env->NewStringUTF(cJson.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_checkoff_ListManager_toggleCheck (JNIEnv *env, jobject, jstring jJson, jstring jListLocation, jstring jItemLocation) {
    //Convert java type variables into c++ type variables
    const char *cppJson = env->GetStringUTFChars(jJson, NULL);
    int cppListLocation = atoi(env->GetStringUTFChars(jListLocation, NULL));
    int cppItemLocation = atoi(env->GetStringUTFChars(jItemLocation, NULL));

    //Build the manager using the Json string
    ListManager manager = ListManagerSerializer(cppJson).manager;

    //Do the appropriate action
    List* curr_list = manager.getListAddress(cppListLocation);
    ToggleCheck toggler(cppItemLocation, *curr_list);

    //Convert manager to c++ json string
    string cJson = ListManagerSerializer(manager).json;

    // Converting C++ String to Java String and return it
    return env->NewStringUTF(cJson.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_checkoff_ListManager_moveItem (JNIEnv *env, jobject, jstring jJson, jstring jListLocation, jstring jItemTo, jstring jItemFrom) {
    //Convert java type variables into c++ type variables
    const char *cppJson = env->GetStringUTFChars(jJson, NULL);
    int cppListLocation = atoi(env->GetStringUTFChars(jListLocation, NULL));
    int cppItemTo = atoi(env->GetStringUTFChars(jItemTo, NULL));
    int cppItemFrom = atoi(env->GetStringUTFChars(jItemFrom, NULL));

    //Build the manager using the Json string
    ListManager manager = ListManagerSerializer(cppJson).manager;

    //Do the appropriate action
    List* curr_list = manager.getListAddress(cppListLocation);
    MoveItem moveItem(cppItemTo, cppItemFrom, *curr_list);

    //Convert manager to c++ json string
    string cJson = ListManagerSerializer(manager).json;

    // Converting C++ String to Java String and return it
    return env->NewStringUTF(cJson.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_checkoff_ListManager_editItem (JNIEnv *env, jobject, jstring jJson, jstring jListLocation, jstring jItemLocation, jstring jItemName, jstring jItemDescription, jstring jItemQty) {
    //Convert java type variables into c++ type variables
    const char *cppJson = env->GetStringUTFChars(jJson, NULL);
    int cppListLocation = atoi(env->GetStringUTFChars(jListLocation, NULL));
    int cppItemLocation = atoi(env->GetStringUTFChars(jItemLocation, NULL));
    string cppItemName(env->GetStringUTFChars(jItemName, NULL));
    string cppItemDescription(env->GetStringUTFChars(jItemDescription, NULL));
    string cppItemQty(env->GetStringUTFChars(jItemQty, NULL));

    //Build the manager using the Json string
    ListManager manager = ListManagerSerializer(cppJson).manager;

    //Do the appropriate action
    List* curr_list = manager.getListAddress(cppListLocation);
    EditItem editItem(cppItemLocation, cppItemName, cppItemDescription, cppItemQty, *curr_list);

    //Convert manager to c++ json string
    string cJson = ListManagerSerializer(manager).json;

    // Converting C++ String to Java String and return it
    return env->NewStringUTF(cJson.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_checkoff_ListManager_removeItem (JNIEnv *env, jobject, jstring jJson, jstring jListLocation, jstring jItemLocation) {
    //Convert java type variables into c++ type variables
    const char *cppJson = env->GetStringUTFChars(jJson, NULL);
    int cppListLocation = atoi(env->GetStringUTFChars(jListLocation, NULL));
    int cppItemLocation = atoi(env->GetStringUTFChars(jItemLocation, NULL));

    //Build the manager using the Json string
    ListManager manager = ListManagerSerializer(cppJson).manager;

    //Do the appropriate action
    List* curr_list = manager.getListAddress(cppListLocation);
    RemoveItem removeItem(cppItemLocation, *curr_list);

    //Convert manager to c++ json string
    string cJson = ListManagerSerializer(manager).json;

    // Converting C++ String to Java String and return it
    return env->NewStringUTF(cJson.c_str());
}