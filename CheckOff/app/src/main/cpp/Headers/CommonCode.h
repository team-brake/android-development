#include <string>
#include <vector> 

using namespace std;

class Item {
public:
	Item(string itemName);
	Item(string itemName, string itemDescription, string itemQty, bool checked = false);
	string getName();
	void setName(string itemName);
	string getDescription();
	void setDescription(string itemDescription);
	string getQty();
	void setQty(string itemQty);
	bool getCheck();
	void toggleCheck();
	//TODO: void saveToDB();
private:
	string name;
	string description;
	string qty;
	bool check;
};

class List {
public:
	List(string listName);
	List(string listName, string listDescription);
	string getName();
	void setName(string listName);
	void setCheckedPositionStart(int pos);
	string getDescription();
	void setDescription(string itemDescription);
	vector<Item> getAllItems();
	Item getItem(int itemLocation);
	void addItem(int itemLocation, string itemName, string itemDescription, string itemQty, bool checked = false);
	void removeItem(int itemLocation);
	void moveItem(int to, int from);
	void editItem(int itemLocation, string name, string description, string qty);
	void toggleItemCheck(int itemLocation);
	int getCheckedPositionStart();
	//TODO: void saveToDB();
private:
	string name;
	string description;
	int checkedPositionStart;  // position where the checked portion of the list begins. This is needed to properly move items to the correct spot when checked off
	vector<Item> items;
};

class ListManager {
public:
	ListManager();
	vector<List> getAllLists();
	void addList(int listLocation, string listName, string listDescription);
	void removeList(int listLocation);
	List * getListAddress(int listLocation);
	List getList(int listLocation);
	void moveList(int to, int from);
	void editList(int listLocation, string listName, string listDescription);
	int getSizeMasterList();
	//TODO: void saveToDB();
private:
	vector<List> masterList;
};

class Action {
public:
	Action();
};

class TrackableAction : public Action {
public:
	TrackableAction();
	virtual void undo(List& currentList) = 0;
};

class AddItem : public TrackableAction {
public:
	AddItem(string itemName, string itemDescription, string itemQty, List& currentList, int itemLocation = -1);
	AddItem(string itemName, string itemDescription, string itemQty, bool checked, List& currentList, int itemLocation = -1);
	void undo(List& currentList) override;
private:
	int prevLocation;
};

class RemoveItem : public TrackableAction {
public:
	RemoveItem(int itemLocation, List& currentList);
	void undo(List& currentList) override;
private:
	string prevName;
	string prevDescription;
	string prevQty;
	bool prevCheckStatus;
	int prevLocation;
};

class EditItem : public TrackableAction {
public:
	EditItem(int itemLocation, string newName, string newDescription, string newQty, List& currentList);
	void undo(List& currentList) override;
private:
	string prevName;
	string prevDescription;
	string prevQty;
	int prevLocation;
};

class ToggleCheck : public TrackableAction {
public:
	ToggleCheck(int itemLocation, List& currentList);
	void undo(List& currentList) override;
private:
	int prevPosition;
	int curPosition;
};

class MoveItem : public TrackableAction {
public:
	MoveItem(int to, int from, List& currentList);
	void undo(List& currentList) override;
private:
	int prevTo;
	int prevFrom;
};

class ActionTracker {
public:
	ActionTracker();
	void pushActionToStack(TrackableAction * toTrack);
	void popActionFromStack(List& currentList);
	void clearStack();
private:
	vector<TrackableAction *> actionStack;
};

class AddList : public Action {
public:
	AddList(string listName, string listDescription, ListManager& currentListManager, int listLocation = -1);
};

class RemoveList : public Action {
public:
	RemoveList(int listLocation, ListManager& currentListManager);
};

class EditList : public Action {
public:
	EditList(int listLocation, string listName, string listDescription, ListManager& currentListManager);
};

class MoveList : public Action {
public:
	MoveList(int to, int from, ListManager& currentListManager);
};