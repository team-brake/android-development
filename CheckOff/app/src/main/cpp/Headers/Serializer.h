#pragma once
#include "CommonCode.h"
#include "../rapidjson/writer.h"
#include "../rapidjson/stringbuffer.h"

using namespace rapidjson;
using namespace std;


class Serializer {
public:
	Serializer();
	virtual void serialize(Writer<StringBuffer>& writer) = 0;
};

class ListManagerSerializer : public Serializer {
private:
	void parse();
public:
	string json;
    ListManager manager;
	ListManagerSerializer(ListManager listManager);
	ListManagerSerializer(string json_str);
	void serialize(Writer<StringBuffer>& writer) override;
};