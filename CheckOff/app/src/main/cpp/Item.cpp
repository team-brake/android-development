#include "CommonCode.h"

Item::Item(string itemName) {
	name = itemName;
	description = "";
	qty = "";
	check = false;
}

Item::Item(string itemName, string itemDescription, string itemQty, bool checked) {
	name = itemName;
	description = itemDescription;
	qty = itemQty;
	check = checked;
}

string Item::getName() {
	return name;
}

void Item::setName(string itemName) {
	name = itemName;
}

string Item::getDescription() {
	return description;
}

void Item::setDescription(string itemDescription) {
	description = itemDescription;
}

string Item::getQty() {
	return qty;
}

void Item::setQty(string itemQty) {
	qty = itemQty;
}

bool Item::getCheck() {
	return check;
}

void Item::toggleCheck() {
	check = !check;
}